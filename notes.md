- Simulator is the host and C++ program is the client
- A socket connection is set up between simulator and the C++ program
- the programs that need to be written to accomplish the project are src/FusionEKF.cpp, src/FusionEKF.h, kalman_filter.cpp, kalman_filter.h, tools.cpp, and tools.h

- Simulator uses the data file obj_pose-laser-radar-synthetic-input.txt. This file has measurements organized as follows:

For Radar:
R, sensor_type, rho_measured, phi_measured, rhodot_measured, timestamp, x_groundtruth, y_groundtruth, vx_groundtruth, vy_groundtruth, yaw_groundtruth, yawrate_groundtruth.

For Lidar:
L, sensor_type, x_measured, y_measured, timestamp, x_groundtruth, y_groundtruth, vx_groundtruth, vy_groundtruth, yaw_groundtruth, yawrate_groundtruth.

- Groundtruth represents the actual path the bicycle took, it is for calculating root mean squared error.
- yaw_groundtruth, yawrate_groundtruth : We do not need to worry about these in this project
